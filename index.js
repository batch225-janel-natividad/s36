//Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

const taskRoute = require("./routes/taskRoute")


// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection 
//Connection to MongoDB atlas

mongoose.connect(`mongodb+srv://janel:${process.env.PASSWORD}@cluster0.yxm0cbf.mongodb.net/MRC?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

//Setup notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open',() => console.log('Connected to MongoDB'));

//Add the task routes
//Allows all the task routes created in the "taskRoute.js" file use "/tasks" route

app.use("/tasks", taskRoute);


//Port connect

app.listen(port, () => console.log(`Now listening to port ${port}`));


